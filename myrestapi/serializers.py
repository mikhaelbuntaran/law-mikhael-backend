from rest_framework import serializers

from .models import GenshinChar

class GenshinCharSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = GenshinChar
		fields = (
			'id',
			'name',
			'element',
			'rarity',
			'owned',
			'level',
			'ascend',
			'constellation'
		)