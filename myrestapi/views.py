from django.shortcuts import render
from rest_framework import viewsets

from .serializers import GenshinCharSerializer
from .models import GenshinChar

# Create your views here.
class GenshinCharViewSet(viewsets.ModelViewSet):
    queryset = GenshinChar.objects.all().order_by('rarity')
    serializer_class = GenshinCharSerializer