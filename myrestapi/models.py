from django.db import models

# Create your models here.
class GenshinChar(models.Model):
	ELEMENTS = (
		('Hydro', 'Hydro'),
		('Pyro', 'Pyro'),
		('Cryo', 'Cryo'),
		('Electro', 'Electro'),
		('Anemo', 'Anemo'),
		('Geo', 'Geo'),
		('Dendro', 'Dendro'),
	)

	RARITY_LIST = (
		('4-star', '4-star'),
		('5-star', '5-star'),
	)

	name = models.CharField(max_length=60)
	element = models.CharField(max_length=10, choices=ELEMENTS)
	rarity = models.CharField(max_length=10, choices=RARITY_LIST, default='4-star')
	owned = models.BooleanField(default=False)
	level = models.PositiveIntegerField(default=0)
	ascend = models.PositiveIntegerField(default=0)
	constellation = models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.name + " (" + self.element + ")"