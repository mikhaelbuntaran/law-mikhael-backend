from django.contrib import admin
from .models import GenshinChar

# Register your models here.
admin.site.register(GenshinChar)